/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufma.sco.telas;

import br.ufma.sco.classes.ModeloTabela;
import br.ufma.sco.dal.ModuloConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author sjodiel
 */
public class TelaAgendamento extends javax.swing.JInternalFrame {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    /**
     * Creates new form TelaAgendamento
     */
    public TelaAgendamento() {
        conexao = ModuloConexao.conector();
        //carregaStatus();
        initComponents();
        
        preencherTable("select * from tbAgenda");
    }

    private void pesquisar_cliente() {

        String sql = "select idcli as ID, nomecli as Nome, fonecli as Fone from tbclientes where nomecli like ?";
        try {

            pst = conexao.prepareStatement(sql);
            pst.setString(1, txtAgeCliente.getText() + "%");
            rs = pst.executeQuery();

            tblAgeCliente.setModel(DbUtils.resultSetToTableModel(rs));

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);

        }
    }

    //metodo para set os campos do formulario com o conteudo da tabela
    public void setar_campos() {

        int setar = tblAgeCliente.getSelectedRow();
        txtAgeIdCliente.setText(tblAgeCliente.getModel().getValueAt(setar, 0).toString());
        //a linha desabilitar o botão adicionar
        //btnCliAdicionar.setEnabled(false);

    }
    
    // metodo para cadastrar
    private void cadastrarAgendamento() {

        String sql = "insert into tbAgenda(data_agenda,hora_agenda,categoria_agenda,situacao_agenda,idcli,iduser) values(?,?,?,?,?,?) ";

        try {

            pst = conexao.prepareStatement(sql);

            pst.setString(1, ftfAgeData.getText());
            pst.setString(2, ftfAgeHora.getText());
            pst.setString(3, cboAgeCategoria.getSelectedItem().toString());
            pst.setString(4, cboAgeSituacao.getSelectedItem().toString());
            pst.setString(5, txtAgeIdCliente.getText());
            pst.setString(6, cboAgeDentista.getSelectedItem().toString());
            
            // validando os campos obrigatorios 
            if (ftfAgeData.getText().isEmpty() || ftfAgeHora.getText().isEmpty() || cboAgeCategoria.getSelectedItem().toString().isEmpty()
                    || cboAgeSituacao.getSelectedItem().toString().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Preencha os todos campos obrigatorios");

            } else {

                //pst.executeUpdate();
                int adicionado = pst.executeUpdate();
                preencherTable("select * from tbAgenda");

                if (adicionado > 0) {

                    JOptionPane.showMessageDialog(null, "Agendamento cadastrado com sucesso");
                    // as linhas limpam os campos
                    txtAgeIdCliente.setText(null);
                    ftfAgeData.setText(null);
                    ftfAgeHora.setText(null);
                    //cboAgeCategoria.setSelectedItem(null);
                    //cboAgeSituacao.setSelectedItem(null);
                    //cboAgeDentista.setSelectedItem(null);
                    //cboUsuAdm.setSelectedItem(null);

                }

            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }
    
    
    public void preencherTable(String sql){
        
        ArrayList dados = new ArrayList();
        
        String [] colunas = new String[]{"ID","Data","Hora","Categoria","Situaçao","Paciente","Dentista"};
        
        try {
            
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                dados.add(new Object[]{rs.getInt("idAgenda"),rs.getString("data_agenda"),rs.getString("hora_agenda"),
                rs.getString("situacao_agenda"),rs.getString("categoria_agenda"),rs.getInt("idcli"),rs.getInt("iduser")
                });
            }
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(null, e);
       
        }
        
        ModeloTabela  tabela = new ModeloTabela(dados, colunas);
        tblListaAgendamento.setModel(tabela);
        tblListaAgendamento.getColumnModel().getColumn(0).setPreferredWidth(1);
        tblListaAgendamento.getColumnModel().getColumn(0).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(1).setPreferredWidth(40);
        tblListaAgendamento.getColumnModel().getColumn(1).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(2).setPreferredWidth(25);
        tblListaAgendamento.getColumnModel().getColumn(2).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(3).setPreferredWidth(65);
        tblListaAgendamento.getColumnModel().getColumn(3).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(4).setPreferredWidth(40);
        tblListaAgendamento.getColumnModel().getColumn(4).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(5).setPreferredWidth(20);
        tblListaAgendamento.getColumnModel().getColumn(5).setResizable(false);
        tblListaAgendamento.getColumnModel().getColumn(6).setPreferredWidth(20);
        tblListaAgendamento.getColumnModel().getColumn(6).setResizable(false);
        tblListaAgendamento.getTableHeader().setResizingAllowed(false);
        //tblListaAgendamento.setAutoResizeMode(tblListaAgendamento.AUTO_RESIZE_OFF);
        tblListaAgendamento.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
    
    }

    
    public void setar_campos_agendamento(){

        int setar = tblListaAgendamento.getSelectedRow();
        ftfAgeData.setText(tblListaAgendamento.getModel().getValueAt(setar, 1).toString());
        ftfAgeHora.setText(tblListaAgendamento.getModel().getValueAt(setar, 2).toString());
        cboAgeCategoria.setSelectedItem(tblListaAgendamento.getModel().getValueAt(setar, 4).toString());
        cboAgeSituacao.setSelectedItem(tblListaAgendamento.getModel().getValueAt(setar, 3).toString());
        txtAgeIdCliente.setText(tblListaAgendamento.getModel().getValueAt(setar, 5).toString());

        cboAgeDentista.setSelectedItem(tblListaAgendamento.getModel().getValueAt(setar, 6).toString());
        //jTextField1.setText(tblListaAgendamento.getModel().getValueAt(setar, 0).toString());

        //a linha desabilitar o botão adicionar
        btnAgeAdicionar.setEnabled(false);

    }
    
        //metodo para alterar usuario
    private void alterarAgendamento() {

        String sql = "update tbAgenda set data_agenda=?,hora_agenda=?,categoria_agenda=?,situacao_agenda=?,iduser=? where idcli=?";

        try {

            pst = conexao.prepareStatement(sql);

            
            pst.setString(1, ftfAgeData.getText());
            pst.setString(2, ftfAgeHora.getText());
            pst.setString(4, cboAgeCategoria.getSelectedItem().toString());
            pst.setString(3, cboAgeSituacao.getSelectedItem().toString());
            //pst.setString(5, txtAgeIdCliente.getText());
            pst.setString(5, cboAgeDentista.getSelectedItem().toString());
            pst.setString(6, txtAgeIdCliente.getText());
            //pst.setString(7, jTextField1.getText());
            // validando os campos obrigatorios 
            if (ftfAgeData.getText().isEmpty() || ftfAgeHora.getText().isEmpty() || cboAgeCategoria.getSelectedItem().toString().isEmpty()
                    || cboAgeSituacao.getSelectedItem().toString().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Preencha os todos campos obrigatorios");

            } else {

                //pst.executeUpdate();
                int adicionado = pst.executeUpdate();
                preencherTable("select * from tbAgenda");

                if (adicionado > 0) {

                    JOptionPane.showMessageDialog(null, "Agendamento alterado com sucesso");
                    
                    // as linhas limpam os campos
                    
                    ftfAgeData.setText(null);
                    ftfAgeHora.setText(null);
                    //cboAgeCategoria.setSelectedItem(null);
                    //cboAgeSituacao.setSelectedItem(null);
                    //cboAgeDentista.setSelectedItem(null);
                    txtAgeIdCliente.setText(null);
          //          jTextField1.setText(null);
                    //cboUsuAdm.setSelectedItem(null);

                }

            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }
    
        private void removerAgendamento(){
        
        int confirma=JOptionPane.showConfirmDialog(null,"Tem certeza que deseja remover?", "Atenção",JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION){
            
            String sql="delete from tbAgenda where idcli=?";
            
            try {
                
                pst = conexao.prepareStatement(sql);
                pst.setString(1, txtAgeIdCliente.getText());
                
                int apagado = pst.executeUpdate();
                preencherTable("select * from tbAgenda");

                if (apagado>0){
                    
                    
                    JOptionPane.showMessageDialog(null, "Agendamento removido com sucesso");
                    ftfAgeData.setText(null);
                    ftfAgeHora.setText(null);
                    txtAgeIdCliente.setText(null);
                    
                }
            } catch (Exception e) {
                
                JOptionPane.showMessageDialog(null, e);
            
            }
            
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlAgeCliente = new javax.swing.JPanel();
        lblAgePesquisar = new javax.swing.JLabel();
        txtAgeCliente = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblAgeCliente = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtAgeIdCliente = new javax.swing.JTextField();
        pnlAdicionarAgendamento = new javax.swing.JPanel();
        lblAgeData = new javax.swing.JLabel();
        lblAgeHora = new javax.swing.JLabel();
        lblAgeCategorai = new javax.swing.JLabel();
        lblAgeSituacao = new javax.swing.JLabel();
        cboAgeCategoria = new javax.swing.JComboBox<>();
        cboAgeSituacao = new javax.swing.JComboBox<>();
        ftfAgeData = new javax.swing.JFormattedTextField();
        ftfAgeHora = new javax.swing.JFormattedTextField();
        lblAgeDentista = new javax.swing.JLabel();
        cboAgeDentista = new javax.swing.JComboBox<>();
        btnAgeAdicionar = new javax.swing.JButton();
        btnAgeAlterar = new javax.swing.JButton();
        btnAgeRemover = new javax.swing.JButton();
        pnlListaAgendamento = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListaAgendamento = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Agendamento");
        setPreferredSize(new java.awt.Dimension(640, 480));

        pnlAgeCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));
        pnlAgeCliente.setPreferredSize(new java.awt.Dimension(220, 228));

        lblAgePesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/pesquisar.png"))); // NOI18N

        txtAgeCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAgeClienteKeyReleased(evt);
            }
        });

        tblAgeCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "ID", "Nome"
            }
        ));
        tblAgeCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tblAgeCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblAgeClienteMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblAgeCliente);
        if (tblAgeCliente.getColumnModel().getColumnCount() > 0) {
            tblAgeCliente.getColumnModel().getColumn(0).setPreferredWidth(10);
        }

        jLabel1.setText("Id");

        txtAgeIdCliente.setEnabled(false);

        javax.swing.GroupLayout pnlAgeClienteLayout = new javax.swing.GroupLayout(pnlAgeCliente);
        pnlAgeCliente.setLayout(pnlAgeClienteLayout);
        pnlAgeClienteLayout.setHorizontalGroup(
            pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAgeClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnlAgeClienteLayout.createSequentialGroup()
                        .addGroup(pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAgeClienteLayout.createSequentialGroup()
                                .addComponent(txtAgeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(3, 3, 3)
                                .addComponent(lblAgePesquisar))
                            .addGroup(pnlAgeClienteLayout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAgeIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 8, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlAgeClienteLayout.setVerticalGroup(
            pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAgeClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtAgeIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(pnlAgeClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAgeCliente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAgePesquisar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlAdicionarAgendamento.setBorder(javax.swing.BorderFactory.createTitledBorder("Adicionar Agendamento"));
        pnlAdicionarAgendamento.setToolTipText("");
        pnlAdicionarAgendamento.setPreferredSize(new java.awt.Dimension(380, 228));

        lblAgeData.setText("Data");

        lblAgeHora.setText("Hora");

        lblAgeCategorai.setText("Categoria");

        lblAgeSituacao.setText("Situação");

        cboAgeCategoria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Consulta", "Avaliação", "Retorno" }));

        cboAgeSituacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Agendado", "Confirmado", "Desmarcou", "Faltou", "Em Atendimento", "Atendido" }));

        try {
            ftfAgeData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            ftfAgeHora.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        lblAgeDentista.setText("Dentista");

        try {
            //pst = conexao.prepareStatement(sql);
            cboAgeDentista.addItem("Selecionar Dentista");

            String sql = "select iduser from tbusuarios where perfil like 'dentista' ";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery(sql);
            while (rs.next()) {
                cboAgeDentista.addItem(rs.getString(1));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR");
        }

        btnAgeAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32adicionar.png"))); // NOI18N
        btnAgeAdicionar.setPreferredSize(new java.awt.Dimension(38, 38));
        btnAgeAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgeAdicionarActionPerformed(evt);
            }
        });

        btnAgeAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32alterar.png"))); // NOI18N
        btnAgeAlterar.setToolTipText("");
        btnAgeAlterar.setPreferredSize(new java.awt.Dimension(38, 38));
        btnAgeAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgeAlterarActionPerformed(evt);
            }
        });

        btnAgeRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32remove.png"))); // NOI18N
        btnAgeRemover.setPreferredSize(new java.awt.Dimension(38, 38));
        btnAgeRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgeRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlAdicionarAgendamentoLayout = new javax.swing.GroupLayout(pnlAdicionarAgendamento);
        pnlAdicionarAgendamento.setLayout(pnlAdicionarAgendamentoLayout);
        pnlAdicionarAgendamentoLayout.setHorizontalGroup(
            pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                                .addComponent(lblAgeSituacao)
                                .addGap(15, 15, 15)
                                .addComponent(cboAgeSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                                .addComponent(lblAgeHora)
                                .addGap(37, 37, 37)
                                .addComponent(ftfAgeHora)
                                .addGap(14, 14, 14))
                            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                                .addComponent(lblAgeDentista)
                                .addGap(18, 18, 18)
                                .addComponent(cboAgeDentista, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                                .addComponent(lblAgeCategorai)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboAgeCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAgeAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAgeRemover, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAgeAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30))
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addComponent(lblAgeData)
                        .addGap(36, 36, 36)
                        .addComponent(ftfAgeData, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pnlAdicionarAgendamentoLayout.setVerticalGroup(
            pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAgeData)
                            .addComponent(ftfAgeData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAgeHora)
                            .addComponent(ftfAgeHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAgeAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAgeDentista)
                    .addComponent(cboAgeDentista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgeAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnAgeRemover, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlAdicionarAgendamentoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboAgeCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAgeCategorai))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAdicionarAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAgeSituacao)
                            .addComponent(cboAgeSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(24, 24, 24))
        );

        pnlListaAgendamento.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista de Agendamento"));
        pnlListaAgendamento.setPreferredSize(new java.awt.Dimension(610, 192));

        tblListaAgendamento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblListaAgendamento.setPreferredSize(new java.awt.Dimension(445, 144));
        tblListaAgendamento.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tblListaAgendamentoAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        tblListaAgendamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblListaAgendamentoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblListaAgendamento);

        javax.swing.GroupLayout pnlListaAgendamentoLayout = new javax.swing.GroupLayout(pnlListaAgendamento);
        pnlListaAgendamento.setLayout(pnlListaAgendamentoLayout);
        pnlListaAgendamentoLayout.setHorizontalGroup(
            pnlListaAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
        );
        pnlListaAgendamentoLayout.setVerticalGroup(
            pnlListaAgendamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlListaAgendamentoLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlAgeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlAdicionarAgendamento, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE))
                    .addComponent(pnlListaAgendamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlListaAgendamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlAgeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlAdicionarAgendamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setBounds(0, 0, 640, 480);
    }// </editor-fold>//GEN-END:initComponents

    private void txtAgeClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAgeClienteKeyReleased
        // TODO add your handling code here:
        pesquisar_cliente();
    }//GEN-LAST:event_txtAgeClienteKeyReleased

    private void tblAgeClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblAgeClienteMouseClicked
        // TODO add your handling code here:
        setar_campos();
    }//GEN-LAST:event_tblAgeClienteMouseClicked

    private void btnAgeAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgeAdicionarActionPerformed
        // TODO add your handling code here:
        cadastrarAgendamento();
    }//GEN-LAST:event_btnAgeAdicionarActionPerformed

    private void tblListaAgendamentoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblListaAgendamentoMouseClicked
        // TODO add your handling code here:
        setar_campos_agendamento();
    }//GEN-LAST:event_tblListaAgendamentoMouseClicked

    private void tblListaAgendamentoAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tblListaAgendamentoAncestorAdded
        // TODO add your handling code here:
        
    }//GEN-LAST:event_tblListaAgendamentoAncestorAdded

    private void btnAgeAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgeAlterarActionPerformed
        // TODO add your handling code here:
        alterarAgendamento();
    }//GEN-LAST:event_btnAgeAlterarActionPerformed

    private void btnAgeRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgeRemoverActionPerformed
        // TODO add your handling code here:
        removerAgendamento();
    }//GEN-LAST:event_btnAgeRemoverActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgeAdicionar;
    private javax.swing.JButton btnAgeAlterar;
    private javax.swing.JButton btnAgeRemover;
    private javax.swing.JComboBox<String> cboAgeCategoria;
    private javax.swing.JComboBox<String> cboAgeDentista;
    private javax.swing.JComboBox<String> cboAgeSituacao;
    private javax.swing.JFormattedTextField ftfAgeData;
    private javax.swing.JFormattedTextField ftfAgeHora;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAgeCategorai;
    private javax.swing.JLabel lblAgeData;
    private javax.swing.JLabel lblAgeDentista;
    private javax.swing.JLabel lblAgeHora;
    private javax.swing.JLabel lblAgePesquisar;
    private javax.swing.JLabel lblAgeSituacao;
    private javax.swing.JPanel pnlAdicionarAgendamento;
    private javax.swing.JPanel pnlAgeCliente;
    private javax.swing.JPanel pnlListaAgendamento;
    private javax.swing.JTable tblAgeCliente;
    private javax.swing.JTable tblListaAgendamento;
    private javax.swing.JTextField txtAgeCliente;
    private javax.swing.JTextField txtAgeIdCliente;
    // End of variables declaration//GEN-END:variables

}
