/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufma.sco.telas;

import br.ufma.sco.classes.ModeloTabela;
import java.sql.*;
import br.ufma.sco.dal.ModuloConexao;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author sjodiel
 */
public class TelaProcedimentos extends javax.swing.JInternalFrame {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    /**
     * Creates new form TelaProcedimentos
     */
    public TelaProcedimentos() {
        conexao = ModuloConexao.conector();
        initComponents();
        preencherTable("select * from tbprocedimentos");
    }

    public void preencherTable(String sql) {

        ArrayList dados = new ArrayList();

        String[] colunas = new String[]{"Codigo", "Nome", "Valor"};

        try {

            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                dados.add(new Object[]{rs.getInt("idProcedimento"), rs.getString("nome_pro"), rs.getInt("valor")});
            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);

        }

        ModeloTabela tabela = new ModeloTabela(dados, colunas);
        tblProcedimentos.setModel(tabela);
        tblProcedimentos.getColumnModel().getColumn(0).setPreferredWidth(1);
        tblProcedimentos.getColumnModel().getColumn(0).setResizable(false);
        tblProcedimentos.getColumnModel().getColumn(1).setPreferredWidth(40);
        tblProcedimentos.getColumnModel().getColumn(1).setResizable(false);
        tblProcedimentos.getColumnModel().getColumn(2).setPreferredWidth(25);
        tblProcedimentos.getColumnModel().getColumn(2).setResizable(false);
        tblProcedimentos.getTableHeader().setResizingAllowed(false);
        //tblListaAgendamento.setAutoResizeMode(tblListaAgendamento.AUTO_RESIZE_OFF);
        //tblProcedimentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    private void setarCampos() {

        int setar = tblProcedimentos.getSelectedRow();
        txtProCodigo.setText(tblProcedimentos.getModel().getValueAt(setar, 0).toString());
        txtProNome.setText(tblProcedimentos.getModel().getValueAt(setar, 1).toString());
        txtProcValor.setText(tblProcedimentos.getModel().getValueAt(setar, 2).toString());
        cboProCategoria.setSelectedItem(tblProcedimentos.getModel().getValueAt(setar, 3).toString());
        //jTextField1.setText(tblListaAgendamento.getModel().getValueAt(setar, 0).toString());

        //a linha desabilitar o botão adicionar
        btnProAdicionar.setEnabled(false);

    }

    // metodo para cadastrar
    private void cadastrarPro() {

        String sql = "insert into tbprocedimentos(nome_pro,valor,categoria_pro) values(?,?,?)";
        try {

            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtProNome.getText());
            pst.setString(2, txtProcValor.getText());
            pst.setString(3, cboProCategoria.getSelectedItem().toString());

            // validando os campos obrigatorios 
            if (txtProNome.getText().isEmpty() || txtProcValor.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Preencha os todos campos obrigatorios");

            } else {

                //pst.executeUpdate();
                int adicionado = pst.executeUpdate();
                preencherTable("select * from tbprocedimentos");

                if (adicionado > 0) {

                    JOptionPane.showMessageDialog(null, "Procedimento adicionado com sucesso");
                    // as linhas limpam os campos
                    txtProCodigo.setText(null);
                    txtProNome.setText(null);
                    txtProcValor.setText(null);
                    //cboUsuAdm.setSelectedItem(null);

                }

            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }

    //metodo para alterar usuario
    private void alterarPro() {

        String sql = "update tbprocedimentos set nome_pro=?,valor=?,cartegoria_pro=?, where idProcedimento=?";
        //String sql = "insert into tbprocedimentos(nome_pro,valor,categoria_pro) values(?,?,?)";

        try {

            pst = conexao.prepareStatement(sql);

            pst.setString(1, txtProNome.getText());
            pst.setString(2, txtProcValor.getText());
            pst.setString(3, cboProCategoria.getSelectedItem().toString());

            // validando os campos obrigatorios 
            if (txtProNome.getText().isEmpty() || txtProcValor.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Preencha os todos campos obrigatorios");

            } else {

                //pst.executeUpdate();
                int adicionado = pst.executeUpdate();
                preencherTable("select * from tbprocedimentos");

                if (adicionado > 0) {

                    JOptionPane.showMessageDialog(null, "Dados do usuário alterado com sucesso");
                    // as linhas limpam os campos
                    txtProCodigo.setText(null);
                    txtProNome.setText(null);
                    txtProcValor.setText(null);

                }

            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }

    }

    //metodo para romover um usuário
    private void removerPro() {

        int confirma = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja remover?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (confirma == JOptionPane.YES_OPTION) {

            String sql = "delete from tbprocedimentos where idProcedimento=?";

            try {

                pst = conexao.prepareStatement(sql);
                pst.setString(1, txtProCodigo.getText());
                

                int apagado = pst.executeUpdate();
                preencherTable("select * from tbprocedimentos");
                if (apagado > 0) {

                    JOptionPane.showMessageDialog(null, "Usuario removido com sucesso");
                    txtProCodigo.setText(null);
                    txtProNome.setText(null);
                    txtProcValor.setText(null);

                }
            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, e);

            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblProNome = new javax.swing.JLabel();
        txtProNome = new javax.swing.JTextField();
        lblProValor = new javax.swing.JLabel();
        txtProcValor = new javax.swing.JTextField();
        lblProCategoria = new javax.swing.JLabel();
        cboProCategoria = new javax.swing.JComboBox<>();
        btnProAdicionar = new javax.swing.JButton();
        btnProAlterar = new javax.swing.JButton();
        btnProDeletar = new javax.swing.JButton();
        lblProCodigo = new javax.swing.JLabel();
        txtProCodigo = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProcedimentos = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Procedimentos");
        setToolTipText("");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Adicionar Procedimento"));
        jPanel1.setPreferredSize(new java.awt.Dimension(305, 320));

        lblProNome.setText("Nome do Procedimento");

        lblProValor.setText("Valor");

        txtProcValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProcValorActionPerformed(evt);
            }
        });

        lblProCategoria.setText("Categoria");

        cboProCategoria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dentistica ", "Cirugia", "Protose", "Radiologia" }));

        btnProAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32adicionar.png"))); // NOI18N
        btnProAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProAdicionarActionPerformed(evt);
            }
        });

        btnProAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32alterar.png"))); // NOI18N
        btnProAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProAlterarActionPerformed(evt);
            }
        });

        btnProDeletar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufma/sco/icones/32remove.png"))); // NOI18N
        btnProDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProDeletarActionPerformed(evt);
            }
        });

        lblProCodigo.setText("Codigo");

        txtProCodigo.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtProcValor, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblProCodigo)
                            .addComponent(txtProNome, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblProCategoria)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboProCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtProCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblProNome)
                                    .addComponent(lblProValor))))
                        .addContainerGap(22, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnProAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnProAlterar)
                        .addGap(47, 47, 47)
                        .addComponent(btnProDeletar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProCodigo)
                    .addComponent(txtProCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addComponent(lblProNome)
                .addGap(1, 1, 1)
                .addComponent(txtProNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblProValor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtProcValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProCategoria)
                    .addComponent(cboProCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(63, 63, 63)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProAlterar)
                    .addComponent(btnProDeletar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProAdicionar))
                .addGap(44, 44, 44))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Procedimentos"));
        jPanel2.setPreferredSize(new java.awt.Dimension(305, 320));
        jPanel2.setRequestFocusEnabled(false);
        jPanel2.setVerifyInputWhenFocusTarget(false);

        tblProcedimentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblProcedimentos.setPreferredSize(new java.awt.Dimension(225, 376));
        tblProcedimentos.getTableHeader().setReorderingAllowed(false);
        tblProcedimentos.setUpdateSelectionOnSort(false);
        tblProcedimentos.setVerifyInputWhenFocusTarget(false);
        tblProcedimentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProcedimentosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProcedimentos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 14, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setBounds(0, 0, 640, 480);
    }// </editor-fold>//GEN-END:initComponents

    private void txtProcValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProcValorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProcValorActionPerformed

    private void btnProAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProAdicionarActionPerformed
        // TODO add your handling code here:
        cadastrarPro();
    }//GEN-LAST:event_btnProAdicionarActionPerformed

    private void btnProAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProAlterarActionPerformed
        // TODO add your handling code here:
        alterarPro();
    }//GEN-LAST:event_btnProAlterarActionPerformed

    private void tblProcedimentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProcedimentosMouseClicked
        // TODO add your handling code here:
        setarCampos();
    }//GEN-LAST:event_tblProcedimentosMouseClicked

    private void btnProDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProDeletarActionPerformed
        // TODO add your handling code here:
        removerPro();
    }//GEN-LAST:event_btnProDeletarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProAdicionar;
    private javax.swing.JButton btnProAlterar;
    private javax.swing.JButton btnProDeletar;
    private javax.swing.JComboBox<String> cboProCategoria;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblProCategoria;
    private javax.swing.JLabel lblProCodigo;
    private javax.swing.JLabel lblProNome;
    private javax.swing.JLabel lblProValor;
    private javax.swing.JTable tblProcedimentos;
    private javax.swing.JTextField txtProCodigo;
    private javax.swing.JTextField txtProNome;
    private javax.swing.JTextField txtProcValor;
    // End of variables declaration//GEN-END:variables
}
