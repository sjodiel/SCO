/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufma.sco.telas;

import java.sql.*;
import br.ufma.sco.dal.ModuloConexao;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author sjodiel
 */
public class Test extends JFrame {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    JComboBox jc = new JComboBox();
    JPanel panel = new JPanel();

    public Test() {

        conexao = ModuloConexao.conector();
        //initComponents(); 

        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            //pst = conexao.prepareStatement(sql);

            String sql = "select iduser, usuario from tbusuarios where perfil like 'dentista' ";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery(sql);
            while (rs.next()) {
                jc.addItem(rs.getString(1) + " === " + rs.getString(2));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR");
        } finally {
            try {
                pst.close();
                rs.close();
                conexao.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "ERROR CLOSE");
            }
        }
        panel.add(jc);
        this.getContentPane().add(panel);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Test();
    }
}
