-- comentários
-- a linha abaixo cria um banco de dados
create database dbsco;
-- a linha abaixo escolge o banco de dados
use dbsco;
-- o bloco de instruções abaixo cria uma tabela
create table tbusuarios(
iduser int primary key,
usuario varchar(50) not null,
fone varchar(15),
login varchar(15) not null unique,
senha varchar(15) not null
);
-- o comando abaixo descreve a tabela
describe tbusuarios;
-- a linha abaixo insere dados na tabela (CRUD)
-- create -> insert
insert into tbusuarios(iduser,usuario,fone,login,senha)
values(1,'José de Assis','9999-9999','joseassis','12345');
-- a linha abaixo exibe os dados da tabela (CRUD)
-- read -> select
select * from tbusuarios;

insert into tbusuarios(iduser,usuario,fone,login,senha)
values(2,'Jodiel','9999-9999','jod','12345');
insert into tbusuarios(iduser,usuario,fone,login,senha)
values(3,'Dentista','9999-9999','dente','12345');

-- a linha abaixo modifica dados na tabel (CRUD)
-- update -> update
update tbusuarios set fone='8888-8888' where iduser=2;
-- a linha abaixo apaga um resgitro da tabela (CRUD);
-- delete -> delete
delete from tbusuarios where iduser=3;

select * from tbusuarios;
-- a linha a seguir deletar o banco de dados 


create table tbclientes(
idcli int primary key auto_increment,
nomecli varchar(50) not null,
endcli varchar(100),
fonecli varchar(50) not null,
emailcli varchar(50)
);
-- adicionar uma nova coluna em uma tabela existente 
ALTER TABLE tbclientes ADD estadocli varchar(50) AFTER endcli;
ALTER TABLE tbclientes ADD historicocli varchar(500) AFTER emailcli;
-- remover coluna
ALTER TABLE tbclientes DROP COLUMN historicocli;
describe tbclientes;

insert into tbclientes(nomecli,endcli,estadocli,fonecli,emailcli)
values('Jodiel Fabricio','Rua Quatro, 12','Maranhão','9999-9999', 'jodielfabricio@gmail.com');

delete from tbclientes where idcli=1; 

select * from tbclientes;
-- DROP table tbhistorico;
create table tbhistorico(
idhis int primary key auto_increment,
data_his timestamp default current_timestamp,
descricao_his varchar(150) not null,
dentista varchar(50) not null,
idcli int not null,
foreign key(idcli) references tbclientes(idcli),
iduser int not null,
foreign key(iduser) references tbusuarios(iduser)
);
describe tbhistorico;
insert into tbhistorico(descricao_his,dentista, idcli,iduser)
values('Jodiel Fabricio','Rua Quatro, 12',1,1);
insert into tbhistorico(descricao_his,dentista, idcli,iduser)
values('Jodiel','Rua Quatro, 12',1,1);
select * from tbhistorico;

-- o codigo abaixo unir informções de tres tabelas
select
H.idhis,data_his,descricao_his,
C.nomecli,
U.usuario
from tbhistorico as H
inner join tbclientes as C
on (H.idcli = C.idcli)
inner join tbusuarios as U
on (H.iduser = U.iduser);

-- adicionar uma nova coluna em uma tabela tbusuario campo perfil 
alter table tbusuarios add column perfil varchar(20) not null;
select * from tbusuarios;
describe tbusuarios;

delete from tbusuarios where iduser=3;

update tbusuarios set perfil='admin' where iduser=1;
update tbusuarios set usuario='Administrador', login='admin', senha='admin' where iduser=1;
update tbusuarios set perfil='user' where iduser=2;

update tbusuarios set perfil='dentista' where iduser=3;

-- a linha abaixo faz uma busca personalizada com filtro
select * from tbclientes where nomecli like 'j%';


create table tbos(
idhis int primary key auto_increment,
data_his timestamp default current_timestamp,
descricao_his varchar(150) not null,
equipamentos varchar(50) not null,
defeitos  varchar(50) not null,
idcli int not null,
foreign key(idcli) references tbclientes(idcli)
);

ALTER TABLE tbos ADD valor decimal(10,2) AFTER defeitos;

describe tbos;